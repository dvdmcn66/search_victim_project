import matplotlib.pyplot as plt
import numpy as np
from pylab import cm
from typing import List
import compute_function

def find_victim(list_factor_div_correction: List[float], list_points: np.ndarray, list_distances: List[float], bias_correction: float, 
                min_max_square: List[float], size: int, respect_unit: bool, min_square: float, max_square: float, 
                show_distance_in_map: bool, victim: List[float], verbose: bool) -> List[float]:
    """Find the victim's position based on a series of distance measurements.

    Args:
        list_factor_div_correction (List[float]): List of correction factors.
        list_points (np.ndarray): List of measured points.
        list_distances (List[float]): List of distances corresponding to measured points.
        bias_correction (float): Correction add for bias.
        min_max_square (List[float]): Minimum and maximum values for the map square.
        size (int): Size of the map.
        respect_unit (bool): Flag to indicate whether to respect the unit.
        min_square (float): Minimum value for the map square.
        max_square (float): Maximum value for the map square.
        show_distance_in_map (bool): Flag to show distances in the map.
        victim (List[float]): True position of the victim.
        verbose (bool): Verbosity flag.

    Returns:
        List[float]: Estimated position of the victim.
    """
    proba_map = []
    max_list = []
    distance_to_victim_list = []
    real_position_found_list = []
    total_prob_sum_list = []
    variance_map_list = []
    proba_map_list = []
    margin_list = []
    for correction_factor in list_factor_div_correction:
        proba_map, margin = compute_function.create_map_product_donut(list_points, list_distances, bias_correction,
                                                                    correction_factor, min_max_square, size, respect_unit)
        margin_list.append(margin)
        matrix_position = list(compute_function.argmax_2d(proba_map))
        real_position_found = compute_function.convert_grid_result(matrix_position, size, min_square, max_square, margin)

        max_value = proba_map[matrix_position[0], matrix_position[1]]
        proba_map_list.append(proba_map)

        max_list.append(max_value)
        distance_to_victim = compute_function.compute_distance(real_position_found, victim)
        real_position_found_list.append(real_position_found)
        distance_to_victim_list.append(distance_to_victim)
        total_gauss_sum = sum([sum(i) for i in proba_map])
        total_prob_sum_list.append(total_gauss_sum)
        variance_map_list.append(np.nanvar(proba_map))

    correction_max, correction_max_distance_power, product_max_distance_power, product_max_list, mean_arg, mean_correction = compute_correction_factor_distance(
        list_factor_div_correction, respect_unit, list_points, max_list, variance_map_list)

    real_position_found = real_position_found_list[mean_arg]
    if verbose:
        display_curves(list_factor_div_correction, distance_to_victim_list, max_list, variance_map_list, product_max_list,
                       product_max_distance_power)

        display_text_res(correction_max, correction_max_distance_power, mean_correction, distance_to_victim_list, mean_arg,
                         max_list, list_points)
        display_map(show_distance_in_map, True, list_points, list_distances, victim, real_position_found,
                    proba_map_list[mean_arg], min_max_square, margin)

    if verbose:
        print("\n\n ----------------------------------------------------------------- \n\n")
    return real_position_found

def display_map(show_distance_in_map: bool, show_map: bool, list_points: np.ndarray, list_distances: List[float], victim: List[float], real_position_found: List[float], proba_map: np.ndarray, min_max_square: List[float], margin: float):
    """Display the map with relevant information."""
    if show_distance_in_map:
        i_label = 0
        for p in list_points:
            plt.text(p[0] + .03, p[1] + .03, list_distances[i_label], fontsize=9)
            i_label += 1
    if show_map:
        plt.scatter(list_points[:, 0], list_points[:, 1])
        plt.scatter(victim[0], victim[1], marker='P', label='Victim')
        plt.scatter(real_position_found[0], real_position_found[1], marker='*', label='Presumed position')

        plt.legend()

        plt.xlabel('X coordinate')
        plt.ylabel('Y coordinate')
        #For a reason I ignore, the heatmap if not well aligned with the points
        plt.title('Victim and Presumed Position (TBD : The heatmap is offsetted)')
        plt.imshow(proba_map, extent=[min_max_square[0] - margin, min_max_square[1] + margin, min_max_square[0] - margin,
                                         min_max_square[1] + margin], cmap=cm.jet, origin='lower')
        plt.colorbar()
        plt.draw()
        plt.figure()

    plt.pause(0.1)

def display_curves(list_factor_div_correction: List[float], distance_to_victim_list: List[float], max_list: List[float], variance_map_list: List[float], product_max_list: List[float], product_max_distance_power: List[float]):
    """Display curves."""
    plt.plot(list_factor_div_correction, distance_to_victim_list / max(distance_to_victim_list), label="distance_to_victim_list (norm 0-1)")
    plt.plot(list_factor_div_correction, max_list / max(max_list), label="max_list")

    plt.plot(list_factor_div_correction, variance_map_list / max(variance_map_list), label="variance_map_list")
    plt.plot(list_factor_div_correction, product_max_list / max(product_max_list), label="product_max_list")
    plt.plot(list_factor_div_correction, product_max_distance_power / max(product_max_distance_power), label="product_max_distance_power")

    plt.legend()
    plt.show(block=True)

    plt.draw()

def display_text_res(correction_max: float, correction_max_distance_power: float, mean_correction: float, distance_to_victim_list: List[float], mean_arg: int, max_list: List[float], list_points: np.ndarray):
    """Display text results."""
    print("correction product_max_list", correction_max,
          "correction product_max_distance_power", correction_max_distance_power,
          "mean correction : ", mean_correction)
    print("distance : ", distance_to_victim_list[mean_arg],
          " VS min distance : ", min(distance_to_victim_list))

    print("max_list : ", max_list)
    print("distance_to_victim_list : ", distance_to_victim_list)

    print("Nb points : ", len(list_points))

def compute_correction_factor_distance(list_factor_div_correction: List[float], respect_unit: bool, list_points: np.ndarray, max_list: List[float], variance_map_list: List[float]) -> tuple[float, float, List[float], List[float], int, float]:
    """Compute the correction factor based on distance."""

    inverse_factor = [1/x for x in list_factor_div_correction]
    power = 1
    if not respect_unit:
        power = len(list_points)
    product_max_distance_power = [max_list[i] * inverse_factor[i]**power for i in range(len(max_list))]
    product_max_list = [max_list[i] * variance_map_list[i] for i in range(len(max_list))]
    arg_max_product_max_list = np.argmax(product_max_list)
    arg_max_product_max_distance_power = np.argmax(product_max_distance_power)
    mean_arg = int((arg_max_product_max_list + arg_max_product_max_distance_power)/2)
    correction_max = list_factor_div_correction[arg_max_product_max_list]
    correction_max_distance_power = list_factor_div_correction[arg_max_product_max_distance_power]
    mean_correction = list_factor_div_correction[mean_arg]
    return correction_max, correction_max_distance_power, product_max_distance_power, product_max_list, mean_arg, mean_correction
