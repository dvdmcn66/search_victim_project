# Distance Experimentation
import matplotlib.pyplot as plt
import numpy as np
from pylab import cm
import random
import compute_function
import victim_search_algorithm

initial_measurements = 3
bias_factor = 2 # The algorithm wants to find it in order to find the victim
bias_add = 0
bias_correction = 1 # if we want to correct bias add manually
factorial_noise = 40 # (in %) 20, will be divided by 1 + uniform(-factor_noise_lim, +factor_noise_lim)/100
additive_noise = 4
min_square = 0
max_square = 40
min_max_square = [min_square, max_square]
size = 200 #pixels
respect_unit = True

victim = compute_function.init_victim(max_square)
list_points, list_distances = compute_function.list_measurements(initial_measurements, max_square, bias_factor, bias_add, victim, factorial_noise, additive_noise)

list_factor_div_correction = [x/4 for x in range(1, 16)]
show_map = True
show_distance_in_map = False
new_points_count = 10

for point_num in range(new_points_count):
    real_position_found = victim_search_algorithm.find_victim(list_factor_div_correction, list_points, list_distances, bias_correction, min_max_square, size, respect_unit,
                                          min_square, max_square, show_distance_in_map, victim, show_map)
    print("Position ", victim, " vs ", real_position_found)
    print("Distance to real position : ", compute_function.compute_distance(victim, real_position_found))
    new_point = [random.uniform(real_position_found[0] - 10, real_position_found[0] + 10),
                 random.uniform(real_position_found[1] - 10, real_position_found[1] + 10)]
    list_points, list_distances = compute_function.add_chosen_measurement(list_points, list_distances, new_point[0], new_point[1],
                                                                         bias_factor, bias_add, victim, factorial_noise,
                                                                         additive_noise)
