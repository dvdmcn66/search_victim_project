import matplotlib.pyplot as plt
import numpy as np
import random
from pylab import cm

def compute_distance(victim: list[float], point: list[float]) -> float:
    """Compute the Euclidean distance between two points."""
    return ((point[0] - victim[0])**2 + (point[1] - victim[1])**2)**0.5

def normalize(vector: list[float]) -> list[float]:
    """Normalize a 2D vector."""
    magnitude = (vector[0]**2 + vector[1]**2)**0.5
    return [vector[0] / magnitude, vector[1] / magnitude]

def make_false_donut_map(mean: list[float], sigma: float, min_max_square: list[float], size: int, margin: float) -> np.ndarray:
    """Create a synthetic donut-shaped map."""
    x, y = np.meshgrid(np.linspace(min_max_square[0] - margin, min_max_square[1] + margin, size), 
                       np.linspace(min_max_square[0] - margin, min_max_square[1] + margin, size))
    distance = np.abs(sigma - np.sqrt((x - mean[0])**2 + (y - mean[1])**2))
    return np.exp(-(distance ** 2 / (2.0 * np.sqrt(abs(sigma)) ** 2)))

def init_random_uniform_2d(max_value: float, min_value: float = 0) -> list[float]:
    """Initialize a random 2D point with uniform distribution."""
    return [random.uniform(min_value, max_value), random.uniform(min_value, max_value)]

def init_victim(map_size: float) -> list[float]:
    """Initialize the victim's position randomly within the map."""
    return init_random_uniform_2d(map_size)

def bias_distance(distance: float, bias_factor: float, bias_add: float, factor_noise_lim: float, add_noise_lim: float) -> float:
    """Apply bias to the distance measurement."""
    factor_noise = 1 + np.random.uniform(-factor_noise_lim, factor_noise_lim, 1) / 100
    add_noise = np.random.uniform(-add_noise_lim, add_noise_lim, 1)
    return distance * bias_factor * factor_noise[0] + bias_add + add_noise[0]

def list_measurements(num_measurements: int, map_size: float, bias_factor: float, bias_add: float, victim: list[float], factor_noise_lim: float, add_noise_lim: float) -> tuple[np.ndarray, list[float]]:
    """Generate a list of points and their biased distance measurements."""
    points = []
    distances = []
    for _ in range(num_measurements):
        points.append(init_random_uniform_2d(map_size))
    for p in points:
        distances.append(bias_distance(compute_distance(victim, p), bias_factor, bias_add, factor_noise_lim, add_noise_lim))
    return np.array(points), distances

def add_chosen_measurement(points: np.ndarray, distances: list[float], x: float, y: float, bias_factor: float, bias_add: float, victim: list[float], factor_noise_lim: float, add_noise_lim: float) -> tuple[np.ndarray, list[float]]:
    """Add a chosen point and its biased distance measurement to the list."""
    point = [x, y]
    points = np.vstack([points, point])
    distances.append(bias_distance(compute_distance(victim, point), bias_factor, bias_add, factor_noise_lim, add_noise_lim))
    return points, distances

def argmax_2d(matrix: np.ndarray) -> tuple[int, int]:
    """Find the coordinates of the maximum value in a 2D matrix."""
    max_index = np.argmax(matrix)
    return divmod(max_index, matrix.shape[1])

def convert_one_coordinate(value: float, size: int, min_square: float, max_square: float, margin: float) -> float:
    """Convert a grid coordinate to real-world coordinates."""
    return (min_square - margin) + (2 * margin + max_square - min_square) * (0.5 + value) / size

def convert_grid_result(grid_result: tuple[int, int], size: int, min_square: float, max_square: float, margin: float) -> list[float]:
    """Convert grid coordinates to real-world coordinates."""
    return [convert_one_coordinate(grid_result[1], size, min_square, max_square, margin), 
            convert_one_coordinate(grid_result[0], size, min_square, max_square, margin)]

def normalize_donut_map(donut_map: np.ndarray, size: int) -> np.ndarray:
    """Normalize the donut map."""
    total = sum(map(sum, donut_map)) / (size**2)
    return donut_map / total

def create_map_product_donut(points: np.ndarray, distances: list[float], bias_correction: float, correction_factor: float, min_max_square: list[float], size: int, respect_unit: bool) -> tuple[np.ndarray, float]:
    """Create the sum of Gaussian maps."""
    corrected_distances = [(distances[i] - bias_correction) / correction_factor for i in range(len(distances))]
    margin = max(corrected_distances) * 2
    for i, point in enumerate(points):
        if i == 0:
            proba_map = normalize_donut_map(make_false_donut_map(point, corrected_distances[i], min_max_square, size, margin), size)
        else:
            proba_map *= normalize_donut_map(make_false_donut_map(point, corrected_distances[i], min_max_square, size, margin), size)
    if respect_unit:
        proba_map = proba_map ** (1 / len(points))
    return proba_map, margin
